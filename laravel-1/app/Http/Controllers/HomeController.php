<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index ()
    {
        
        return view('home');
    }

    public function post ( Request $request)
    {
        $fname = $request['firstname'];
        $lname = $request['lastname'];
        
        return view('welcome', compact('fname','lname'));
    }
}
