<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::post('/welcome', 'HomeController@post');
Route::get('/register', 'RegisterController@index');
Route::get('/master', function () {
    return view('layouts.master');
});

Route::get('/table', function () {
    return view('layouts.table');
});

Route::get('/data-table', function () {
    return view('layouts.data-table');
});

Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast/{cash_id}', 'CastController@show');
Route::get('/cast/{cash_id}/edit', 'CastController@edit');
Route::put('/cast/{cash_id}', 'CastController@update');
Route::delete('/cast/{cash_id}', 'CastController@destroy');
